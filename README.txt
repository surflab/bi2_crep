
This code has been tested on Ubuntu 16.04

Input: .obj file describing the discrete representation of the (2d) microstructure.
Output: A bv file of the microstructure with G-Splines around each edge.

Dependencies:

OpenMesh: https://www.openmesh.org/download/
Follow the instructions to build and install it on your system : https://www.openmesh.org/media/Documentations/OpenMesh-Doc-Latest/a03933.html


To build:

mkdir Build
cd Build
cmake ../Source
make

To run:
cd Build/GraphScaffolding
./GenScaffolding $FILENAME

There are example test files in the "data" subfolder of GraphScaffolding, i.e.
./GenScaffolding data/trihex.off will build the scaffolding for the trihex.off file

To view the resulting bv file, use BezierView: https://www.cise.ufl.edu/research/SurfLab/bview/
A copy of the executable is in the data file, if it does not work, try either a different version or the web app at the above link.

License:

bi2_crep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. The GNU GPL license can be found at http://www.gnu.org/licenses/ or in the included file GNU_GPL_License.txt.
