% derive a scaffold for a C0 surface (total degree 2, 4-split)

% assume central vertex is origin

shw=3; clf;
nbrs = 4;  % due to graph connectivity

% --- create some neighbors -- this would be from the graph xy locations
ii=1:nbrs; ang = ii*2*pi/nbrs; nbr = 4*[cos(ang); sin(ang)];
nbr(:,1) = nbr(:,1)*1; % make asym
sc = 1/2; % choice: derive 4 base points from 4 neighbors
nxt = [2:nbrs 1];
% --- create the 4 base points at the node of interest
bas0 = (sc/2)*(nbr+nbr(:,nxt));

% --- create the 2 base points at each of 4 neighbors
for ii=1:nbrs, 
   if ii==nbrs, ip=1; else ip = ii+1; end; 
   if ii==1, im=nbrs; else im = ii-1; end; 
      bas{ii}(:,1) = (1-sc)*nbr(:,ii) + (sc/2)*nbr(:,im);
      bas{ii}(:,2) = (1-sc)*nbr(:,ii) + (sc/2)*nbr(:,ip);
end;

%  4 base points at central node and 2 from neighbor determine 
%  a tube that fits C0 with neighbors

dg = 2; % not sufficient for smooth

% CHANGE HERE v
load 'scf22.dat';  % <----- piecewise bi- degree 2 -----

zup = 0.5; % scale in z-direction %zz = zup*[1, -1 , -1 , 1];
ow = 1/4; % width of oct  1/6 wider

trs = 4; % number of total degree patches per quad
sym = 4; % number of quads per edge
% CHANGE HERE v
ncf = (dg+1)^2; % number of coefficients (for bi-degree 2)
nvs = 6; % number of input points (4 from point and 2 from neighbor)
for sec=1:nbrs, 
   inp = [bas{sec},bas0]; %  size:  xy  X  nvs 
   for tb = 1:sym,
         % CHANGE HERE  v
         lo = (tb-1)*ncf+1;
         hi = lo-1+ncf;
         % CHANGE HERE  v
         mat = scf22(lo:hi,:);
         p{sec}{tb}(:,1) = mat(:,1:nvs)*inp(1,:)';
         p{sec}{tb}(:,2) = mat(:,1:nvs)*inp(2,:)';
         p{sec}{tb}(:,3) = mat(:,nvs+1)*zup; 
   end;
   bas0 = bas0(:,nxt);
end;

if shw==3,
   colr = ['y','c','m','r'];

   % CHANGE HERE  v
   idx = [1,2,5,4; 2,3,6,5];
   idx = [idx; 3+idx];
   nidx = 4;
   for sec=1:nbrs, 
      for tb = 1:sym,
            for ii=1:nidx,
               for i0=1:3,
                  x{i0} = [p{sec}{tb}(idx(ii,1),i0);...
                           p{sec}{tb}(idx(ii,2),i0);...
                           p{sec}{tb}(idx(ii,3),i0);...
                           p{sec}{tb}(idx(ii,4),i0)];
               end;
               patch(x{1},x{2},x{3},colr(tb)); hold on;
            end;
      end;
   end;
   axis equal;
   view(3);
end;

if shw==4,  % output to Bezierview
   fid = fopen('scf22.bv','w');
   for sec=1:4,
   for tb=1:sym, % 
      % CHANGE HERE  v
      ll = 1; fprintf(fid,'4 2\n');
      for lv=0:dg,
      for jj=0:dg,
         for xyz=1:3, % xyz
            fprintf(fid,'%f ', p{sec}{tb}(ll,xyz)); 
         end;
         fprintf(fid,'\n'); 
         ll = ll+1;
      end;
      end;
   end;
   end;
   fclose(fid);
end;

