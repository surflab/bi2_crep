// scaffolding.cpp

/*

This file is part of bi2_crep. 




bi2_crep is free software: you can redistribute it and/or modify

it under the terms of the GNU General Public License as published by

the Free Software Foundation, either version 3 of the License, or

(at your option) any later version.




bi2_crep is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of

MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

GNU General Public License for more details.




You should have received a copy of the GNU General Public License

along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

#include "scaffolding.hpp"

using namespace std;

Scaffolding::Scaffolding(Mesh mesh, int type)
{
    patch_type=type;
    myMesh=mesh;
    volume=0.0;

    if(patch_type==BI22)
    {
        read_data("data/BI22/scf22.dat", DATA);
        read_data("data/BI22/scfvol22.dat", VOLUME); // Need to generate new data for this
    }
    else if(patch_type==TOTAL_DEG2)
    {
        read_data("data/TOTAL_DEG2/scf.dat", DATA);
        read_data("data/TOTAL_DEG2/scfvol.dat", VOLUME);
    }

    find_min_max();
    gen_bas_points();

    for(auto v_it=myMesh.vertices_begin(); v_it!=myMesh.vertices_end(); ++v_it)
    {
        if(myMesh.valence(*v_it)==4)
        {
            surfaces.push_back(gen_scaffold(*v_it));
        }
    }
    write_bv_file();
}

void Scaffolding::print_vector_of_points(const vector<point>& points)
{
    for(const auto& p : points)
    {
        cout << fixed << setprecision(2) << get<0>(p) << " " << get<1>(p) << " " << get<2>(p) << endl;
    }
}

void Scaffolding::read_data(const char* filename, const int type)
{
    ifstream data_file(filename);
    assert(data_file.is_open());

    vector<datum> data;
    string line;
    while(getline(data_file, line))
    {
        istringstream temp{line};
        datum vals;
        for(const int& i : properties.data_per_input)
        {
            double val;
            temp >> val;
            vals.push_back(val);
        }
        data.push_back(vals);
    }
    gen_data_matrix(data, type);
}

void Scaffolding::gen_data_matrix(vector<datum>& data, const int type)
{
    int rows=data.size();
    int cols=0;
    Eigen::MatrixXd matrix;
    if(type==DATA)
    {
        cols=(properties.data_per_input).size();
        matrix=Eigen::MatrixXd(rows, cols);
    }
    else if(type==VOLUME)
    {
        cols=(properties.num_input_points).size();
        matrix=Eigen::MatrixXd(rows, cols);
    }
    for(int row=0; row<rows; ++row)
    {
        for(int col=0; col<cols; ++col)
        {
            matrix(row, col)=(data[row])[col];
        }
    }
    if(type==DATA){
        data_matrix=matrix;
    }
    else if(type==VOLUME){
        volume_data_matrix=matrix;
    }
}

void Scaffolding::find_min_max(){
    x_min=x_max=(myMesh.point((Mesh::VertexHandle)0))[0];
    y_min=y_max=(myMesh.point((Mesh::VertexHandle)0))[1];
    for(auto v_it=myMesh.vertices_begin(); v_it!=myMesh.vertices_end(); ++v_it)
    {
        if((myMesh.point(*v_it))[0]<x_min){
            x_min=(myMesh.point(*v_it))[0];
        }
        else if((myMesh.point(*v_it))[0]>x_max){
            x_max=(myMesh.point(*v_it))[0];
        }
        if((myMesh.point(*v_it))[1]<y_min){
            y_min=(myMesh.point(*v_it))[1];
        }
        else if((myMesh.point(*v_it))[1]>y_max){
            y_max=(myMesh.point(*v_it))[1];
        }
   }
}

void Scaffolding::gen_bas_points()
{
    myMesh.add_property(bas_points);
    
    double sc=properties.sc;
    Mesh::HalfedgeHandle previous;
    Mesh::Point to_vertex;
    Mesh::Point origin;
    Mesh::Point prev_vertex;
    Mesh::Point bas;
    point local_bas;

    for(auto he_it=myMesh.halfedges_begin(); he_it!=myMesh.halfedges_end(); ++he_it)
    {
        // Associate with each halfedge the bas points closest to it's  "from_vertex"
        previous=myMesh.prev_halfedge_handle(*he_it);

        origin=myMesh.point(myMesh.from_vertex_handle(*he_it));
        to_vertex=myMesh.point(myMesh.to_vertex_handle(*he_it));
        prev_vertex=myMesh.point(myMesh.from_vertex_handle(previous));

        if(properties.vary_thickness)
        {
            double x_weight=(x_max-origin[0])/(x_max-x_min);
            double y_weight=(y_max-origin[1])/(y_max-y_min);

            double weighted_sc=1.0 - (x_weight+y_weight)/2.0;
            bas=(1.0-weighted_sc)*origin+(weighted_sc/2.0)*prev_vertex+(weighted_sc/2.0)*to_vertex;
        }
        else{
            bas=(1.0-sc)*origin+(sc/2.0)*prev_vertex+(sc/2.0)*to_vertex;
        }

        get<0>(local_bas)=bas[0];
        get<1>(local_bas)=bas[1];
        get<2>(local_bas)=bas[2];

        double fx=0.025*(bas[0]*bas[0]+2*bas[1]*bas[1]);
        get<2>(local_bas)=fx;

        myMesh.property(bas_points, *he_it)=local_bas;
    }




    // Change just one vertex
    auto he=(Mesh::HalfedgeHandle)144;
    previous=myMesh.prev_halfedge_handle(he);
    origin=myMesh.point(myMesh.from_vertex_handle(he));
    to_vertex=myMesh.point(myMesh.to_vertex_handle(he));
    prev_vertex=myMesh.point(myMesh.from_vertex_handle(previous));

    if(properties.vary_thickness)
    {
        double x_weight=(x_max-origin[0])/(x_max-x_min);
        double y_weight=(y_max-origin[1])/(y_max-y_min);

        double weighted_sc=1.25;// - (x_weight+y_weight)/10.0;
        bas=(1.0-weighted_sc)*origin+(weighted_sc/2.0)*prev_vertex+(weighted_sc/2.0)*to_vertex;
    }
    else{
        bas=(1.0-sc)*origin+(sc/2.0)*prev_vertex+(sc/2.0)*to_vertex;
    }

    get<0>(local_bas)=bas[0];
    get<1>(local_bas)=bas[1];
    get<2>(local_bas)=bas[2];

    double fx=0.025*(bas[0]*bas[0]+2*bas[1]*bas[1]);
    get<2>(local_bas)=fx;

    myMesh.property(bas_points, he)=local_bas;



}

scaffold Scaffolding::gen_scaffold(const Mesh::VertexHandle base_vertex)
{
    Base_Points bases;
    scaffold surface;

    if(myMesh.valence(base_vertex)!=4)
    {
        cout << "Error!  Vertex " << base_vertex << " is not valence 4!\n";
    }
    
    gen_bases(bases, base_vertex);
    initialize_surface(surface);

    gen_patches(surface, bases);
    volume+=find_volume(bases);

    return surface;
}

void Scaffolding::gen_bases(Base_Points& bases, const Mesh::VertexHandle base_vertex)
{
    vector<Mesh::VertexHandle> neighbors;
    find_neighbors(neighbors, base_vertex);

    gen_initial_base(bases, neighbors, base_vertex);
    gen_neighbor_bases(bases, neighbors, base_vertex);
}

void Scaffolding::find_neighbors(vector<Mesh::VertexHandle>& neighbors, Mesh::VertexHandle base_vertex)
{
    int num_neighbors=4;
    neighbors.reserve(num_neighbors);

    Mesh::HalfedgeHandle halfedge=myMesh.halfedge_handle(base_vertex);
  
    for(auto v_it=myMesh.vv_iter(base_vertex); v_it.is_valid(); ++v_it)
    {
        neighbors.push_back(*v_it);
    }
}

void Scaffolding::gen_initial_base(Base_Points& bases, const vector<Mesh::VertexHandle>& neighbors, const Mesh::VertexHandle base_vertex)
{
    Mesh::Point origin=myMesh.point(base_vertex);
    point neighbor;
    for(auto he_it=myMesh.voh_iter(base_vertex); he_it.is_valid(); ++he_it)
    {
       (bases.initial).push_back(myMesh.property(bas_points, *he_it));
    }
    // TODO: Do something more elegant than this rotate
    rotate((bases.initial).begin(), (bases.initial).begin()+1, (bases.initial).end());
}

void Scaffolding::gen_neighbor_bases(Base_Points& bases, const vector<Mesh::VertexHandle>& neighbors, const Mesh::VertexHandle base_vertex)
{
    Mesh::Point origin=myMesh.point(base_vertex);
    point neighbor;

    OpenMesh::HalfedgeHandle opposite;
    OpenMesh::HalfedgeHandle opposite_next;

    for(auto he_it=myMesh.vih_iter(base_vertex); he_it.is_valid(); ++he_it)
    {
        opposite=myMesh.opposite_halfedge_handle(*he_it);
        opposite_next=myMesh.next_halfedge_handle(opposite);
        (bases.neighbors).push_back(myMesh.property(bas_points, opposite_next));
        (bases.neighbors).push_back(myMesh.property(bas_points, *he_it));
    }
}

void Scaffolding::set_ip_and_im(int& ip, int& im, const int i)
{
    int num_neighbors=(properties.neighbors).size();
    if(i==(num_neighbors-1))
    {
        ip=0;
    }
    else
    {
        ip=i+1;
    }

    if(i==0)
    {
        im=num_neighbors-1;
    }
    else
    {
        im=i-1;
    }
}

void Scaffolding::initialize_surface(scaffold& surface)
{
    if(patch_type==BI22)
    {
        surface.resize(1);
        for(auto &edge : surface)
        {
            edge.resize((properties.neighbors).size());
        }
        for(auto& edge : surface)
        {
            for(auto& quad : edge)
            {
                quad.resize((properties.quads_per_edge).size());
            }
        }
    }
    else if(patch_type==TOTAL_DEG2)
    {
        surface.resize((properties.neighbors).size());
        for(auto &edge : surface)
        {
            edge.resize((properties.quads_per_edge).size());
        }
        for(auto& edge : surface)
        {
            for(auto& quad : edge)
            {
                quad.resize((properties.patches_per_quad).size());
            }
        }
    }
}

void Scaffolding::gen_patches(scaffold& surface, Base_Points& bases)
{
    Eigen::MatrixXd imp(3, (properties.num_input_points).size());
    Eigen::MatrixXd mat; // names of matrices from Matlab code
    Eigen::MatrixXd result;
    Base_Points local_base=bases;
    int low, high;
    int degree=2;
    int ncf=(degree+1)*(degree+1);
    int nvs=6;
    int dummy=-1;

    for(const int& edge : properties.neighbors)
    {
        gen_imp(imp, edge, local_base);
        for(const int& quad : properties.quads_per_edge)
        {
            if(patch_type==BI22)
            {
                set_bounds(low, high, quad+1, dummy);
                gen_mat(mat, low, high);

                // X
                result=((mat.block(0,0,mat.rows(),mat.cols()-1))*((imp.row(0)).transpose()));
                set_control_points_x(surface, Scaffold_Index(edge, quad, dummy), result);

                // Y
                result=((mat.block(0,0,mat.rows(),mat.cols()-1))*((imp.row(1)).transpose()));
                set_control_points_y(surface, Scaffold_Index(edge, quad, dummy), result);

                // Z
                result=mat.col(mat.cols()-1)*properties.zup; // Flat
                //result=((mat.block(0,0,mat.rows(),mat.cols()-1))*((imp.row(2)).transpose()))+mat.col(mat.cols()-1)*properties.zup; // Surface
                set_control_points_z(surface, Scaffold_Index(edge, quad, dummy), result);
            }

            else if(patch_type==TOTAL_DEG2)
            {
                for(const int& patch_in_quad : properties.patches_per_quad)
                {
                    set_bounds(low, high, quad+1, patch_in_quad+1);
                    gen_mat(mat, low, high);
                    Eigen::MatrixXd result;

                    // X
                    result=((mat.block(0,0,mat.cols()-1,mat.rows()))*((imp.row(0)).transpose()));
                    set_control_points_x(surface, Scaffold_Index(edge, quad, patch_in_quad), result);

                    // Y
                    result=((mat.block(0,0,mat.cols()-1,mat.rows()))*((imp.row(1)).transpose()));
                    set_control_points_y(surface, Scaffold_Index(edge, quad, patch_in_quad), result);

                    // Z
                    result=mat.col(mat.cols()-1)*properties.zup;
                    set_control_points_z(surface, Scaffold_Index(edge, quad, patch_in_quad), result);
                }
            }
        }
    }
}

void Scaffolding::gen_imp(Eigen::MatrixXd& imp, const int section, Base_Points& base)
{
    imp(0, 0)=get<0>((base.neighbors)[2*section]);
    imp(1, 0)=get<1>((base.neighbors)[2*section]);
    imp(2, 0)=get<2>((base.neighbors)[2*section]);

    imp(0, 1)=get<0>((base.neighbors)[2*section+1]);
    imp(1, 1)=get<1>((base.neighbors)[2*section+1]);
    imp(2, 1)=get<2>((base.neighbors)[2*section+1]);

    for(const int& i : properties.neighbors)
    {
        imp(0, i+2)=get<0>((base.initial)[i]);
        imp(1, i+2)=get<1>((base.initial)[i]);
        imp(2, i+2)=get<2>((base.initial)[i]);
        //cout << imp(2, i+2) << endl;
    }
    rotate((base.initial).begin(), (base.initial).begin()+1, (base.initial).end());
}

void Scaffolding::set_bounds(int& low, int& high, const int quad, const int patch_in_quad)
{
    int sizeof_num_coeff=(properties.num_coeff).size();
    int sizeof_patches_per_quad=(properties.patches_per_quad).size();

    int first_term=(quad-1)*sizeof_patches_per_quad*sizeof_num_coeff;
    int second_term=(patch_in_quad-1)*sizeof_num_coeff;

    if(patch_type==BI22)
    {
        int degree=2;
        int ncf=(degree+1)*(degree+1);

        low=(quad-1)*ncf+1;
        high=low-1+ncf;

        low=low-1;
    }


    else if(patch_type==TOTAL_DEG2)
    {
        low=first_term+second_term+1;
        high=low+sizeof_num_coeff;

        // Subtract 1 b.c. Matlab is 1-indexed, c++ is 0-indexed
        low=low-1;
        high=high-1;
    }
}

void Scaffolding::gen_mat(Eigen::MatrixXd& mat, const int low, const int high)
{
    // data_matrix has 7 cols, 96 rows
    int num_cols=data_matrix.cols();
    int num_rows=high-low;
    mat=data_matrix.block(low, 0, num_rows, num_cols);
}


/*
 *  The next three function can be combined with some code refactoring
 *  Problem: get<> cannot take an int variable as a parameter
 *  i.e. int index=1; get<index>surface is illegal
 *  Must use a different data structure than tuple to do this
 *
 *  I thought I'd try a tuple for the points, but this turned out 
 *  to be more difficult than expected...
 *  In the future: use tuples ONLY when you need different types of
 *  data in each position
 *  in hindsight: a fixed size array (with a wrapper?) would have been perfect
 */

void Scaffolding::set_control_points_x(scaffold& surface, const Scaffold_Index& index, const Eigen::MatrixXd& point_mat)
{
    if(patch_type==BI22)
    {
        surface[0][index.edge][index.quad].resize(point_mat.rows());
        for(int i=0; i<point_mat.rows(); ++i)
        {
            get<0>(surface[0][index.edge][index.quad][i])=point_mat(i,0)/32.0;
        }
    }
    else if(patch_type==TOTAL_DEG2)
    {
        surface[index.edge][index.quad][index.patch_in_quad].resize(point_mat.rows());
        for(int i=0; i<point_mat.rows(); ++i)
        {
            get<0>(surface[index.edge][index.quad][index.patch_in_quad][i])=point_mat(i,0)/32.0;
        }
    }
}

void Scaffolding::set_control_points_y(scaffold& surface, const Scaffold_Index& index, const Eigen::MatrixXd& point_mat)
{
    if(patch_type==BI22)
    {
        surface[0][index.edge][index.quad].resize(point_mat.rows());
        for(int i=0; i<point_mat.rows(); ++i)
        {
            get<1>(surface[0][index.edge][index.quad][i])=point_mat(i,0)/32.0;
        }
    }
    else if(patch_type==TOTAL_DEG2)
    {
        surface[index.edge][index.quad][index.patch_in_quad].resize(point_mat.rows());
        for(int i=0; i<point_mat.rows(); ++i)
        {
            get<1>(surface[index.edge][index.quad][index.patch_in_quad][i])=point_mat(i,0)/32.0;
        }
    }
}

void Scaffolding::set_control_points_z(scaffold& surface, const Scaffold_Index& index, const Eigen::MatrixXd& point_mat)
{
    if(patch_type==BI22)
    {
        surface[0][index.edge][index.quad].resize(point_mat.rows());
        for(int i=0; i<point_mat.rows(); ++i)
        {
            /*double x=get<0>(surface[0][index.edge][index.quad][i]);
            double y=get<1>(surface[0][index.edge][index.quad][i]);
            double z_scale=0.1;
            double fxy=(point_mat(i,0)/32.0)+z_scale*(1.0*x*x+2.0*y*y);
            get<2>(surface[0][index.edge][index.quad][i])=fxy;*/
            get<2>(surface[0][index.edge][index.quad][i])=point_mat(i,0)/32.0;
        }
    }
    else if(patch_type==TOTAL_DEG2)
    {
        surface[index.edge][index.quad][index.patch_in_quad].resize(point_mat.rows());
        for(int i=0; i<point_mat.rows(); ++i)
        {
            get<2>(surface[index.edge][index.quad][index.patch_in_quad][i])=point_mat(i,0)/32.0;
        }
    }
}

void Scaffolding::copy_and_shift(vector<Mesh::VertexHandle>& points, const vector<Mesh::VertexHandle>& original, const int offset)
{
    points.clear();
    points.reserve(original.size());
    for(int i=0; i<original.size(); ++i)
    {
        points.push_back(original[i]);
    }
    rotate(points.begin(), points.begin()+offset, points.end());
}

double Scaffolding::find_volume(Base_Points& bases)
{
    if(patch_type==TOTAL_DEG2 || patch_type==BI22)
    {
        Base_Points local_base=bases;
        Eigen::MatrixXd imp(2, 6);
        double local_volume=0.0;
        for(const int& edge : properties.neighbors)
        {
            gen_imp(imp, edge, local_base);
            for(const int& i : properties.num_input_points)
            {
                for(const int& j : properties.num_input_points)
                {
                    local_volume+=volume_data_matrix(i,j)*imp(0,i)*imp(1,j);
                }
            }
        }
        if(patch_type==TOTAL_DEG2)
        {
            local_volume=properties.zup*(local_volume/(double)11520);
        }
        else
        {
            local_volume=properties.zup*(local_volume/(double)1440);
        }
        return local_volume;
    }
    else
    {
        return -1;
    }
}

void Scaffolding::translate_by_base_vertex(scaffold& surface, const Mesh::VertexHandle base_vertex)
{
    int ll;
    Mesh::Point base=myMesh.point(base_vertex);
    for(const int& edge : properties.neighbors)
    {
        for(const int& quad : properties.quads_per_edge)
        {
            for(const int& patch_in_quad : properties.patches_per_quad)
            {
                ll=0;
                for(const int& i : properties.order)
                {
                    for(int j=0; j<((properties.order).size()-i); ++j)
                    {
                        get<0>(surface[edge][quad][patch_in_quad][ll])+=base[0];
                        get<1>(surface[edge][quad][patch_in_quad][ll])+=base[1];
                        get<2>(surface[edge][quad][patch_in_quad][ll])+=base[2];
                        ll=ll+1;
                    }
                }
            }
        }
    }
}

void Scaffolding::write_bv_file()
{
    ofstream bv_file;
    bv_file.open("scf.bv");
    int ll;
    double shift_amount=0.5;
    double z_shift;
    vector<double> x_shift{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    vector<double> y_shift{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    int layers=10;
    
    if(patch_type==BI22)
    {
        vector<int> order={0,1,2};
        for(int layer=0; layer<layers; ++layer)
        {
            for(scaffold& surface : surfaces)
            {
                for(const int& edge : properties.neighbors)
                {
                    for(const int& quad : properties.quads_per_edge)
                    {
                        ll=0;
                        bv_file << "4 2\n";
                        for(const int& i : order)
                        {
                            for(const int& j : order)
                            {
                                bv_file << (get<0>(surface[0][edge][quad][ll])+x_shift[layer]) << " ";
                                bv_file << (get<1>(surface[0][edge][quad][ll])+y_shift[layer]) << " ";
                                bv_file << (get<2>(surface[0][edge][quad][ll])+z_shift) << " ";
                                bv_file << "\n";
                                ll=ll+1;
                            }
                        }
                    }
                }
            }
            z_shift+=shift_amount;
        }
    }
    else if(patch_type==TOTAL_DEG2)
    {
        for(int layer=0; layer<layers; ++layer)
        {
            for(scaffold& surface : surfaces)
            {
                for(const int& edge : properties.neighbors)
                {
                    for(const int& quad : properties.quads_per_edge)
                    {
                        for(const int& patch_in_quad : properties.patches_per_quad)
                        {
                            ll=0;
                            bv_file << "3 2\n";
                            for(const int& i : properties.order)
                            {
                                for(int j=0; j<((properties.order).size()-i); ++j)
                                {
                                    bv_file << get<0>(surface[edge][quad][patch_in_quad][ll]) << " ";
                                    bv_file << get<1>(surface[edge][quad][patch_in_quad][ll]) << " ";
                                    bv_file << (get<2>(surface[edge][quad][patch_in_quad][ll])+z_shift) << " ";
                                    bv_file << "\n";
                                    ll=ll+1;
                                }
                            }
                        }
                    }
                }
            }
            z_shift+=shift_amount;
        }
    }

    bv_file.close();
}

