// scaffolding.hpp

/*

This file is part of bi2_crep. 




bi2_crep is free software: you can redistribute it and/or modify

it under the terms of the GNU General Public License as published by

the Free Software Foundation, either version 3 of the License, or

(at your option) any later version.




bi2_crep is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of

MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

GNU General Public License for more details.




You should have received a copy of the GNU General Public License

along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef SCAFFOLD
#define SCAFFOLD

#define PI 3.1415925359

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <tuple>
#include <math.h>
#include <iomanip>
#include <algorithm>
#include <assert.h>
#include <Eigen/Core>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>


typedef std::tuple<double, double, double> point;
typedef std::vector<double>                datum;
typedef std::vector<std::vector<point>>    patch;
typedef std::vector<std::vector<patch>>    scaffold;
typedef OpenMesh::PolyMesh_ArrayKernelT<>  Mesh;

struct Scaffold_Properties
{
    double sc=0.5;//0.3333333; // must have 0 < sc < 1, "seed" for barycentric coordinates
    double zup=0.333333333; // again from Matlab code, "z-up"
    bool vary_thickness=false;
    // iterate through vectors via "for(const int& i : vector){}"
    std::vector<int> order={0,1,2}; // deg=2=>C0; 3=>G1 (in the future)
    std::vector<int> neighbors=       {0,1,2,3};
    std::vector<int> patches_per_quad={0,1,2,3};     // trs
    std::vector<int> quads_per_edge=  {0,1,2,3};     // sym
    std::vector<int> num_coeff=       {0,1,2,3,4,5}; // ncf
    std::vector<int> num_input_points={0,1,2,3,4,5}; // nvs
    std::vector<int> data_per_input=  {0,1,2,3,4,5,6}; 
};

struct Base_Points
{
    std::vector<point> initial;
    std::vector<point> neighbors;
};

struct Scaffold_Index
{
    int edge;
    int quad;
    int patch_in_quad;
    Scaffold_Index(int a, int b, int c) :
        edge(a),
        quad(b),
        patch_in_quad(c)
    {}
};

class Scaffolding
{
public:
    enum {DATA, VOLUME, TOTAL_DEG2, BI22};
    Scaffolding(Mesh, int type=BI22);
private:
    // Data
    const Scaffold_Properties properties;
    int patch_type;
    Mesh myMesh; 
    Eigen::MatrixXd data_matrix;
    Eigen::MatrixXd volume_data_matrix;
    OpenMesh::HPropHandleT<point> bas_points;
    std::vector<scaffold> surfaces;
    double volume;
    double x_min;
    double x_max;
    double y_min;
    double y_max;

    scaffold gen_scaffold(Mesh::VertexHandle);
    void write_bv_file();

    // Member Functions
    void print_vector_of_points(const std::vector<point>&);
    void read_data(const char*, const int);
    void gen_data_matrix(std::vector<datum>&, const int);
    void find_min_max();
    void gen_bas_points();

    void gen_bases(Base_Points&, const Mesh::VertexHandle);
    void find_neighbors(std::vector<Mesh::VertexHandle>&, const Mesh::VertexHandle);
    void gen_initial_base(Base_Points&, const std::vector<Mesh::VertexHandle>&, const Mesh::VertexHandle);
    void gen_neighbor_bases(Base_Points&, const std::vector<Mesh::VertexHandle>&, const Mesh::VertexHandle);
    void set_ip_and_im(int&, int&, const int);

    void initialize_surface(scaffold&);
    void gen_patches(scaffold&, Base_Points&);
    void copy_and_shift(std::vector<Mesh::VertexHandle>&, const std::vector<Mesh::VertexHandle>&, const int);
    void gen_imp(Eigen::MatrixXd&, const int, Base_Points&);
    void set_bounds(int&, int&, const int, const int);
    void gen_mat(Eigen::MatrixXd&, const int, const int);
    void set_control_points_x(scaffold&, const Scaffold_Index&, const Eigen::MatrixXd&);
    void set_control_points_y(scaffold&, const Scaffold_Index&, const Eigen::MatrixXd&);
    void set_control_points_z(scaffold&, const Scaffold_Index&, const Eigen::MatrixXd&);
    void translate_by_base_vertex(scaffold&, const Mesh::VertexHandle);

    double find_volume(Base_Points&);
};

#endif

