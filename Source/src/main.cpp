// main.cpp

/*

This file is part of bi2_crep. 




bi2_crep is free software: you can redistribute it and/or modify

it under the terms of the GNU General Public License as published by

the Free Software Foundation, either version 3 of the License, or

(at your option) any later version.




bi2_crep is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of

MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

GNU General Public License for more details.




You should have received a copy of the GNU General Public License

along with this program. If not, see <http://www.gnu.org/licenses/>.

*/


#include "scaffolding.hpp"
#include <string>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

using namespace std;

typedef OpenMesh::PolyMesh_ArrayKernelT<> Mesh;

int main(int argc, char** argv)
{
    if(argc!=2)
    {
        cout << "Correct usage: " << argv[0] << " filename.off\n";
        return 0;
    }
    Mesh myMesh;
    OpenMesh::IO::read_mesh(myMesh, argv[1]);

    Scaffolding scaffolding(myMesh);


    return 0;
}
